#ifndef _P_String_H_
#define _P_String_H_

/*
 * project: String
 * created: Tue Aug 15 10:30:24 2017
 * creator: christian
*/

#include <stdlib.h>
#include <stdio.h>

typedef struct {
  char *value;
  unsigned short size;
  unsigned int pages;
  long len;
} String;


#ifndef PAGESIZE
#define PAGESIZE 4096
#endif

#include <stdio.h>
#include <stdlib.h>

/**************** START PROTOTYPING ****************/
int add(String *str, char add);
/*
 * returns -1 if there is no memory aviavle to concate the struct String
 * 0 by succsess
*/

int init(String *newStr);
/*
 * prepares the string for use

 * returns -1 if there is no memory aviavle to concate the struct String
 * 0 by succsess
 */

void strFree(String *del);
/*
 * deletes the string
 */

int concat(String *des, char *source);
 /*
  * adds source to des

  * returns -1 by by a memory error
 */

void removeTok(String *str, unsigned int index);
/*
 * remove a the token at index, but does not clean any memory
 */

char *substring(String *str, unsigned int beginIndex, unsigned int endIndex);

/*
 * returns NULL if there is an memory error

 * the returned pointer has to cleaned up 
 */

/**************** END   PROTOTYPING ****************/

#endif /*  p_string.h */
