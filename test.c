/*
 * project: String
 * created: Tue Aug 15 11:09:10 2017
 * creator: christian
*/

#include <stdio.h>
#include "p_string.h"

int main(int argc, char *argv[]) {

		String str;
		if (init(&str)) {
			fprintf(stderr, "FAIL\n");
			return -1;
		}

		int i = 0;
		while (i < 3) {
			add(&str, getchar());
			i++;
		}
		concat(&str, "abgehen");
		printf("%s %d %d\n", str.value, str.len, str.size);
		printf("index to remove:\n");

		scanf("%d", &i);
		removeTok(&str, i);
		printf("%s %d %d\n", str.value, str.len, str.size);
		printf("enter start index\n");
		scanf("%d", &i);
		printf("enter end idex\n");
		int end;
		scanf("%d", &end);
		printf("i:%d end:%d\n", i, end);
		char *sub = substring(&str, i, end);
		printf("substring: %s\n", sub);
		free(sub);
		strFree(&str);
	return 0;
}
