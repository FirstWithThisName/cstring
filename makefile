
 # project: string
 # created: Tue Aug 15 11:54:35 2017
 # creator: christian


PROG = p_stringTest #this is the program name
CC = gcc #this is the compiler
OBJ = test.o p_string.o 

$(PROG): $(OBJ)
	$(CC) -o $(PROG) $(OBJ)

p_string.o: p_string.c
	$(CC) -c p_string.c -o p_string.o

test.o: test.c
	$(CC) -c test.c -o test.o

clean:
	rm *.o
